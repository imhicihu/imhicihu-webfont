![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

Mainly, a font for internal use (as a way to automatize some graphical tasks), then a way to by-pass Google penalization of website accesing. Then, once passed a myriad of _bona-fide_ criticism it can convert in an open source, time it will judge!

### What is this repository for? ###

* Quick summary
    - Creation of a webfont to minimize our bandwidth to access the [IMHICIHU](http://www.imhicihu-conicet.gob.ar/) website. 
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - _In the making_
* Configuration
    - [Fontforge](https://fontforge.github.io/en-US/downloads/)
* Dependencies
    - No dependencies
* Database configuration
    - No database was or it will be made for this project
* How to run tests
    - Once you visit [IMHICIHU](http://www.imhicihu-conicet.gob.ar/) website, the browser will download the webfont to your computer, hence, once you visit us again it will load more swiftly
* Deployment instructions
    - Once it is available, just download it from our `Download` section

### Related repositories ###

* Some repositories linked with this project:
     - [Cuneiform fonts migration from pc to mac environment (and viceversa)](https://bitbucket.org/imhicihu/cuneiform-fonts-migration-from-pc-to-mac-environment-and/src/)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/imhicihu-webfont/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/imhicihu-webfont/commits/) section for the current status

### Contribution guidelines ###

* Writing tests
    - _In the making_
* Code review
    - _In the making_
* Other guidelines
    - There is no other guidelines or wiki available

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/imhicihu-webfont/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/imhicihu-webfont/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### Licence ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png).  