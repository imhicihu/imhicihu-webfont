* Software:
    - Backup:
        - [Duplicati](https://www.duplicati.com/) (open-source backup software)
    - Testing & debugging:
        - [CanIuse](https://caniuse.com/): browser/browsing test tool
        - [Jest](https://facebook.github.io/jest/en/) (javascript testing tool)
    - Code editor
        - [Atom](https://atom.io) (code editor) plus lots of packages
        - [Brackets](http://brackets.io/) (code editor)
    - Image editing
        - [ImageOptim](https://github.com/ImageOptim/ImageOptim): image optimization
    - Font management:
        - [FontBase](https://fontba.se/)
    - Font conversion
        - [Fontplop](http://www.fontplop.com/): an OSX/macOS application which takes ttf and otf files and outputs a webfont bundle: `woff2`, `woff`, `ttf`, `svg`, and `eot`
    - Font testing
        - [Font inter/lab](https://rsms.me/inter/lab/?sample=Default&repertoireOrder=&size=13&weight=400&letterSpacing=0&antialias=greyscale&text-decoration=none&text-transform=none&variantLigatures=normal&variantCaps=normal&variantNumeric=normal&rasterizePhrase=Account%20expiration): font testing, kerning _et alia_

* Hardware
    - In this project we use this hardware (not mandatory):
        - Macbook 13"